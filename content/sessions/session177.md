---
title: "جلسه ۱۷۷"
description: "هم‌افزایی شماره صفر"
date: "1398-07-11"
author: "مریم بهزادی"
draft: false
categories:
    - "sessions"
summaryImage: "/img/posters/poster177.jpg"
readmore: false
---
[![poster177](../../img/posters/poster177.jpg)](../../img/poster177.jpg)