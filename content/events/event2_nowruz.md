---
title: "سال نو مبارک"
draft: false
description: "پیام تبریک عید نوروز ۹۷"
author: "مریم بهزادی"
date: "1397-01-01"
summaryImage: "/img/events/nowruz.jpg"
readmore: false
---

[![nowruz](../../img/events/nowruz.jpg)](../../img/events/nowruz.jpg)
