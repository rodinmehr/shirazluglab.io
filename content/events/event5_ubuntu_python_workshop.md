---
title : "کارگاه گنو/لینوکس  و پایتون"
draft : false
description: "کارگاه تخصصی و عملی لینوکس و پایتون"
author: "مریم بهزادی"
date: "1397-09-05"
summaryImage: "/img/events/gnulinux.jpg"
readmore: true
---

[![joomla](../../img/events/gnulinux.jpg)](../../img/events/gnulinux.jpg)

فایل های مربوط به این کارگاه را می توانید از
[اینجا](https://framagit.org/shirazlug/resources/tree/master/presentations/workshop-ubuntu-python)
یا به صورت جداگانه
[فایل ارائه گنو/لینوکس](https://www.slideshare.net/ShirazLUG/ubuntu-workshop)
و
[فایل ارائه پایتون](https://www.slideshare.net/ShirazLUG/python-workshop-124135963)
دانلود کنید.
